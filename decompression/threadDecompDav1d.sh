#!/bin/bash

if [ $# -ne 0 ];
then
    echo "Usage: threadDecomtests"
    exit 1
fi


testDecompFile ()
{
    nbFrameThreads=(1 2 4 6 8)
    nbTileThreads=(1 2 4)
    for tt in ${nbTileThreads[@]};
    do
        for ft in ${nbFrameThreads[@]};
        do
            decompTime=`{ time -p $DAV1D_PATH/dav1d --tilethreads=$tt --framethreads=$ft -i $1 --muxer null -o /dev/null; } 2>&1`

            decompTimeReal=`echo "$decompTime" | grep "real"`
            decompTimeReal=`sed -E 's/(0m|s)//g;s/,/\./g' <<< $decompTimeReal`
            decompTimeReal=${decompTimeReal:4}

            decompTimeUser=`echo "$decompTime" | grep "user"`
            decompTimeUser=`sed -E 's/(0m|s)//g;s/,/\./g' <<< $decompTimeUser`
            decompTimeUser=${decompTimeUser:4}

            logOutput=`echo "$1 $tt $ft $decompTimeReal $decompTimeUser"`
            echo "$logOutput"
            echo "$logOutput" >> decomp_dav1d.log
        done
    done
}

rm decomp_dav1d.log

for f in *.ivf;
do
    testDecompFile $f
done

