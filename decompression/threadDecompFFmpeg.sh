#!/bin/bash

if [ $# -ne 0 ];
then
    echo "Usage: threadDecomtests"
    exit 1
fi


testDecompFile ()
{
    nbThreads=(1 2 4 6 8)
    for t in ${nbThreads[@]};
    do
        decompTime=`{ time -p ffmpeg -thread_type frame -threads $t -i $1 -f null -v error -nostats - 2>&1; } 2>&1`

        decompTimeReal=`echo "$decompTime" | grep "real"`
        decompTimeReal=`sed -E 's/(0m|s)//g;s/,/\./g' <<< $decompTimeReal`
        decompTimeReal=${decompTimeReal:4}

        decompTimeUser=`echo "$decompTime" | grep "user"`
        decompTimeUser=`sed -E 's/(0m|s)//g;s/,/\./g' <<< $decompTimeUser`
        decompTimeUser=${decompTimeUser:4}

        logOutput=`echo "$1 - $t $decompTimeReal $decompTimeUser"`
        echo "$logOutput"
        echo "$logOutput" >> decomp_ffmpeg.log
    done
}

rm decomp_ffmpeg.log

for f in *.mp4;
do
    testDecompFile $f
done

for f in *.webm;
do
    testDecompFile $f 
done

