#!/bin/bash

if [ $# -ne 2 ];
then
    echo "Usage: makeInputSample inputFile outputFile"
    exit 1
fi

$FFMPEG_PATH/ffmpeg -ss 00:05:45 -i $1 -vf "format=yuv420p,scale=1920:1080" -t 00:00:25 $2

