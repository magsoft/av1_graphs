#!/bin/bash

if [ $# -ne 2 ];
then
    echo "Usage: genX264 inputFile outputDir"
    exit 1
fi

CRF=(20 24 25 30 35 40)

for i in ${CRF[@]};
do
    $FFMPEG_PATH/ffmpeg -i $1 -y -c:v libx265 -preset veryslow -x265-params "keyint=120;wpp=1" -tune psnr -crf $i ${2}_crf${i}.mp4
done

