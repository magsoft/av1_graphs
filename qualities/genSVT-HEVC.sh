#!/bin/bash

if [ $# -ne 2 ];
then
    echo "Usage: genVVT inputFile outputDir"
    exit 1
fi

CRF=(20 25 30 35 40)

for i in ${CRF[@]};
do
    ffmpeg -i $1 -nostdin -f rawvideo -pix_fmt yuv420p - | $SVT_PATH/HevcEncoderApp -i stdin -n 1500 -w 1920 -h 1080 -b ${2}_crf${i}.hevc -fps 60 -tune 1 -q $i -encMode 0
done

