#!/bin/bash

if [ $# -ne 2 ];
then
    echo "Usage: genX264 inputFile outputDir"
    exit 1
fi

CRF=(20 25 26 30 35 40)

for i in ${CRF[@]};
do
    $FFMPEG_PATH/ffmpeg -i $1 -y -c:v libx264 -preset veryslow -g 120 -tune psnr -crf $i ${2}_crf${i}.mp4
done

