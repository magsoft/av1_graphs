#!/bin/bash

if [ $# -ne 2 ];
then
    echo "Usage: genVP9 inputFile outputDir"
    exit 1
fi

CRF=(20 28 36 44 52 60 68)

for i in ${CRF[@]};
do
    $AOM_PATH/aomenc -t 8 --end-usage=q --passes=2 --kf-max-dist=120 --cpu-used=1 --aq-mode=0 --tile-columns=2 --cq-level=$i -o ${2}_crf${i}.webm $1
done

