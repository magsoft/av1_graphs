#!/bin/bash

if [ $# -ne 2 ];
then
    echo "Usage: genVVT inputFile outputDir"
    exit 1
fi

#CRF=(20 25 30 35 40)
CRF=(43 46 50)

for i in ${CRF[@]};
do
    #ffmpeg -i $1 -nostdin -f rawvideo -pix_fmt yuv420p - | $SVT_AV1_PATH/Av1EncoderApp -i stdin -n 1500 -w 1920 -h 1080 -b ${2}_crf${i}.ivf -fps 60 -q $i -encMode 1
    $SVT_AV1_PATH/Av1EncoderApp -i $1 -n 1500 -w 1920 -h 1080 -b ${2}_crf${i}.ivf -fps 60 -q $i -encMode 1
d
done

