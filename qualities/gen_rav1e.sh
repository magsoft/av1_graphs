#!/bin/bash

if [ $# -ne 2 ];
then
    echo "Usage: geni_rav1e inputFile outputDir"
    exit 1
fi

CRF=(200 150 100 50 25)

for i in ${CRF[@]};
do
    $RAV1E_PATH/rav1e $1 -o ${2}_crf${i}.ivf --quantizer $i -s 0
done

