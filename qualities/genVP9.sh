#!/bin/bash

if [ $# -ne 2 ];
then
    echo "Usage: genVP9 inputFile outputDir"
    exit 1
fi

CRF=(20 28 36 44 52 60 63)

for i in ${CRF[@]};
do
    $VPX_PATH/vpxenc -t 8 --codec=vp9 --end-usage=q --passes=2 --kf-max-dist=120 --cpu-used=0 --good --tune=psnr --aq-mode=0 --tile-columns=2 --frame-parallel=0 --cq-level="$i" -o ${2}_crf${i}.webm $1
done

